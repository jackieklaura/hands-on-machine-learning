# References

This section will be extended throughout the course. If you have good materials you want to share, please post them
into our chat, and I will integrate them here.

## Core Topic

* Google: [Machine Learning Crash Course with TensorFlow APIs](https://developers.google.com/machine-learning/crash-course) (~14h, interactive online course)
* FreeCodeCamp, Kylie Ying: [Machine Learning for Everybody](https://www.freecodecamp.org/news/machine-learning-for-everybody/) (~4h video)
* Sarah Ciston: [A Critical Field Guide for Working with Machine Learning Datasets](https://knowingmachines.org/critical-field-guide)
* Janelle Shane (2020): [You look like a thing and I love you](https://permalink.obvsg.at/KAW/AC16520769) (book available @ lib)
* Hannah Fry (2019): [Hello world : how to be human in the age of the machine](https://permalink.obvsg.at/KAW/AC15577828) (book available @ lib)
* Pit Noack & Sophia Sanner (2023): [Künstliche Intelligenz verstehen: eine spielerische Einführung in die KI](https://permalink.obvsg.at/KAW/AC16782304) (book available @ lib, also as [e-book](https://supa.uni-ak.ac.at/permalink/f/1qs7pj3/KAW_alma5195870560004523))
  * Freely-accessible website accompanying the book with the interactive code examples: https://www.maschinennah.de/ki-buch/
* [MENACE](https://www.mscroggs.co.uk/menace/) - the Machine Educable Noughts And Crosses Engine
  * Stand-up Maths on YouTube: [MENACE: the pile of matchboxes which can learn](https://youtu.be/R9c-_neaxeU)
* Stefan Seegerer: [This Is How Machines Learn! (Part 1)](https://medium.com/@stefanseegerer/this-is-how-machines-learn-part-1-an-introduction-of-the-underlying-ideas-in-machine-learning-d259a7bdbaf8) and following parts 2-5
* Jay Alammar: [A Visual and Interactive Guide to the Basics of Neural Networks](https://jalammar.github.io/visual-interactive-guide-basics-neural-networks/)

## Python Basics

* w3schools: [Python Tutorial](https://www.w3schools.com/python/default.asp) (Tutorial, well suited for beginners)
* FreeCodeCamp: [Scientific Computing with Python](https://www.freecodecamp.org/learn/scientific-computing-with-python/) (Full and extensive interactive online course)
* Al Sweigart: [Automate the Boring Stuff with Python](https://automatetheboringstuff.com/) (A whole book on Python basics and what you can do with it to make your digital life easier - also freely accessible as a web page)

## Official Python Documentation

* For an overview: [Python 3.11.1 documentation](https://docs.python.org/3/)
* As a first entry point: [The Python Tutorial](https://docs.python.org/3/tutorial/index.html)
* As a reference:
    * [The Python Language Reference](https://docs.python.org/3/reference/index.html)
    * [The Python Standard Library](https://docs.python.org/3/library/index.html)

## Tools

* [Thonny](https://thonny.org/) - Python IDE for beginners
* [PyCharm](https://www.jetbrains.com/pycharm/) - more advanced IDE for developers
* [Jupyter notebooks & more](http://jupyter.org/) - web docs with interactive Python snippets (usable on your own device as a pip module, or on the jupyter.org site - Google's Colab is also just a version of Jupyter notebooks)

## Extended Materials

Extended materials on topics beyond what we will be doing in the course:

* FreeCodeCamp: [Machine Learning with Python](https://www.freecodecamp.org/learn/machine-learning-with-python/) (Full and extensive interactive online course - would fit into our core course topic, but vastly exceeds it workload wise)
* Jay Alammar: [The Illustrated Stable Diffusion](https://jalammar.github.io/illustrated-stable-diffusion)
* Youtube, Computerphile: [How AI Image Generators Work (Stable Diffusion / Dall-E)](https://www.youtube.com/watch?v=1CIpzeNxIhU) (~18min Video)
* Youtube, Computerphile: [Stable Diffusion in Code (AI Image Generation)](https://www.youtube.com/watch?v=-lz30by8-sU) (~17min Video)
* Youtube, Emergent Garden: [Watching Neural Networks Learn](https://www.youtube.com/watch?v=TkwXa7Cvfr8) (~25min Video)
