# Notes on grading

As defined in the course description, and elaborated on in our kick-off session,
your final grade will be based on:

* 40%: Attendance & active participation  
* 30%: Coding exercises  
* 30%: Final project

This will translate into up to 100 points, which translate into a grade as
follows:

* 80-100 points: 1
* 70-80 points: 2
* 60-70 points: 3
* 50-60 points: 4
* 0-50 points: 5

Attendance will not be counted on physical presence at individual sessions, but
on participation in our discourses. Because it is not really possible to assess
this objectively, the points for attendance & active participation will be counted
based on 8 questions posted throughout the semester in our base chat channel. Any
participation in this discourse (that goes beyond a simple "I agree") within a
week of when the question is posted, will be counted as 5 points towards this
category.

We will have 3 exercises, which you solve by writing and submitting runnable
code. For each handed in exercise you get 10 points (it should be runnable
out of the box, but minor errors are ok too - there is no qualitative assessment
here, just submit the code to get the points).

For the final project 30 points can be obtained, distributed across:
- you hand in either a runnable prototype or a concept outline
- there is a comprehensible description of what your prototype/concept does
  or is supposed to do
- you work with (or point out) interesting data sets for your prototype, that
  have not been touched on in the course - or you use data from the course in
  an unexpected way to create new outcomes

## Notes on the final project

As the (ECTS) scope of the course is quite limited, the idea of the final project
is really more of an idea, up to a working prototype. So you can either use what
we applied in the course to some other interesting data set and make your machine
learn something about the data. Or you just have an idea of what would be interesting,
based on certain data sets, and how you would build that into a bigger project -
if you would have the time for that. In that case you just need to outline your
idea and the data you have investigated.

So there is no clear requirement for handing in something, as long as you hand
in something that explains itself (e.g. by well commented code), or that includes
a README file or some other text file outlining your ideas and approach. In case
you are unsure, please connect on base chat and let's figure out together what
your "project" could be.

**Deadline:** the general deadline for your projects is the **end of July**.<br>
As usual, please just write beforehand, if you need an extension.

```{note}
While I want to encourage you to get the stuff done early, some of our summer plans just don't line up.
So in case your actual focused summer work times are more in August or September, that can also be arranged.
```

## Notes on nitpickyness and transparency

This grading scheme is applied not because I think one can reasonably quantify
the genuine work you do and bring into this course, but precisely because one
cannot do so, neither qualitatively nor quantitatively.

And I believe your grade should not depend on whether I as your lecturer think
you did good work or not. Therefore, my aim is to be very explicit and
transparent about how to get to a specific grade. This way everyone get the
possibility to tune their own workloads and strategies in order to get the grade
they want or need.

Consensual exceptions from this framework can always be created. This is a
matter of communication though. If something does not work out for you, but
you have a good alternative solution, please talk to me, and we can discuss it.
