# Session 10: Neural networks and course/project reflections

This session will shortly introduce you to the concept of neural networks and how to implement them from scratch.
We then will reflect upon what we did in the course so far and what we could/would like to apply ML for.
Due to the lack of remaining sessions this session might feel a bit convoluted (not in the sense of neural networks).
The aim is to kick off your own further explorations into ML.

General outline of the session:
* 10min: Arrival and discussion of recent sessions
* 40min: Neural network concepts and implementation
* 25min: Course reflection and project ideas
* 15min: Open discussion, project outlook and info on remaining sessions

## Neural networks

In this short primer, we will go through the following resources:
* Kylie Ying : [Machine Learning for Everybody](https://www.freecodecamp.org/news/machine-learning-for-everybody/), 
  specifically the following parts:
  * [Training Model](https://youtu.be/i_LwzRVP7bg?t=1197) (20:41 to 30:45)
  * [Neural networks](https://youtu.be/i_LwzRVP7bg?t=5984) (1:40:00 to 1:47:55)
* James Loy: [How to build your own Neural Network from scratch in Python](https://towardsdatascience.com/how-to-build-your-own-neural-network-from-scratch-in-python-68998a08e4f6)

As a more detailed further reading resource I also suggest to work through the following tutorial on Real Python:
* Déborah Mesquita: [Python AI: How to Build a Neural Network & Make Predictions](https://realpython.com/python-ai-neural-network/)

## Course reflection and project ideas

We will first do a **talking-by-walking** exercise for 15min, in groups of three. The idea is to just walk and take
turns in talking (and by that reflecting) about a specific issue. So every person should get roughly 5 minutes to think
and talk about the following questions:

* what did you learn in this course, that you might not have learned on your own?
* what didn't you learn (so far), that you thought you might get out of this course?
* what would you now like to try to do with machine learning (within and beyond the course)?

Try to stick to a 3x5 minute schedule, where one person is in the talking-and-reflecting role. It is also ok to just
walk for some time, without talking. But the other two participants can also ask questions if they feel it would
help the talking person to continue with their reflections. And also make sure to be back in the seminar room after
the 15 minutes.

After the talking-by-walking we continue with a **silent discussion** on three flip-chart papers in the room, with the
following topics:
* (Machine-)Learnings
* Still missing
* What would be interesting to apply ML to/for?

After around 10 minutes of the silent discussion, we switch to an open plenary discussion and also talk about the
final project and the upcoming sessions.
