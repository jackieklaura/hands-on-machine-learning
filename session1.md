# Kick-off session

In this session we get to know each other as well as the course outline and the
(expected/assumed/planned) trajectory. As a general disclaimer, this course
is currently in a sort of experimental / beta state, so things will be
produced on the fly, and the outline might change based on our collective
experiences and experimentation.

This session will be held jointly with the kick-off session of the
[Machine Learning](https://base.uni-ak.ac.at/courses/2024S/S05080/) seminar by Paul Feigelfeld.

The slides are available at [slides/session1.html](https://tantemalkah.at/machine-learning/2024/slides/session1.html)
