from kmeans_data import dataset
from math import sqrt
import random


class KMeansClustering:
    k = 5
    dataset = []
    cluster_data = []
    cluster_centers = []

    def __init__(self, dataset, k=5):
        """Initialise the algorithm with a dataset and the k value"""
        self.k = k
        self.dataset = dataset
        # k random points from the data set have to be chosen as initial centers
        indexes = random.sample(range(len(self.dataset)), k)
        self.cluster_centers = [dataset[idx] for idx in indexes]

    def cluster(self):
        """Put each data point into the nearest of the k clusters"""
        self.cluster_data = [[] for _ in range(self.k)]
        for item in self.dataset:
            min_center = 0
            min_distance = self.get_distance(item, self.cluster_centers[min_center])
            for idx in range(1, self.k):
                distance = self.get_distance(item, self.cluster_centers[idx])
                if distance < min_distance:
                    min_center = idx
                    min_distance = distance
            self.cluster_data[min_center].append(item)

    def set_new_means(self):
        """Calculate new cluster means based on the current cluster data."""
        for idx in range(self.k):
            count = len(self.cluster_data[idx])
            x_sum = sum([item[0] for item in self.cluster_data[idx]])
            y_sum = sum([item[1] for item in self.cluster_data[idx]])
            self.cluster_centers[idx] = [x_sum / count, y_sum / count]

    def get_distance(self, item1, item2):
        """Returns the distance between to data points."""
        dx = item1[0] - item2[0]
        dy = item1[1] - item2[1]
        return sqrt(dx**2 + dy**2)


# Now lets use our algorithm to iteratively find better-fitting clusters
MAX_EPOCHS = 20
K = 5

# Initialise the algorithm and create the first clusters (with random centers)
kmeans = KMeansClustering(dataset, K)
kmeans.cluster()

# Now continually improve the clustering
for n in range(MAX_EPOCHS+1):
    print('--------------')
    print(f'Epoch {n} / {MAX_EPOCHS}, Cluster centers: {kmeans.cluster_centers}')
    print(f'Distribution of cluster data points: {[len(cluster) for cluster in kmeans.cluster_data]}')
    kmeans.set_new_means()
    # TODO: here we should insert a check if the new cluster centers are the same as the old ones
    #       in which case we reached a stable result and can stop further calculations
    kmeans.cluster()

# print the final result
print()
print('Result:')
print('=======')
print('centers:', kmeans.cluster_centers)
print('clusters:')
for cluster in kmeans.cluster_data:
    print(cluster)

# TODO: here we could use matplotlib to visualise the final result
