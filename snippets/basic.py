'''
This is a multi-line comment at the beginning of this Python
script. Its aim is to introduce you to the most basic concepts
in Python. We'll walk through this live in the course. If you
look at this in advance and everything here makes perfect sense
to you, and you could recreate it yourself from scratch, feel
free to skip the first hour of our Setup & Python basics session.
'''

# also a comment, only a single line comment
# althought it is followed by just another one
# no machine code will be executed up to (down) here

# but now, let's define some variables
my_boolean = True
my_boolean = False  # well, now we've overwritten the content
my_boolean = 'Not a boolean at all, but a string now!'
my_boolean = True or False  # ahm ... what's that?

# let's check and print the content to the console
print(my_boolean)

print('How can we explain that?')
my_explanation = 'Well, True or False is always ' + str(my_boolean)
print(my_explanation)

# combine printing and evaluation
print('But True and False is always', True and False)

# Let's get some user input
print()  # do an empty line first
user_name = input('Hey! What\'s your name? ')
print(f'Hi {user_name}, nice to talk to you.\n')  # using a format string and adding an empty line at the end


# before we can do more awesome stuff we need some better ideas
# of what types we can use here
my_string1 = 'Well, we already know this one'
my_string2 = "Double-quotes are also fine"
my_string3 = ''
my_string4 = 'The last one was an empty string. Still a string!'

my_multiline_string = '''This string starts here,
but spans several lines.
Quite convenient.'''

my_tedious_multiline_string = 'This string starts here,\n'
my_tedious_multiline_string += 'but spans several lines.\n'
my_tedious_multiline_string += 'Quite inconvenient doing it this way.'

print(my_multiline_string)
print(my_tedious_multiline_string, '\n')

print('So' + 'yes,' + 'strings' + 'can' + 'be' + 'concatenated!')  # whats up with the spaces?

# ok, we got strings, what about numbers and calculations?
my_int = 42
my_float = 42.0000000000001
my_float2 = 42.0
print(f'The type of {my_int} is {type(my_int)}.')
print(f'The type of {my_float2} is {type(my_float2)}.')

# let's do some calculations
print(5 * (12-8) + -15)
print(98 + (59872 / (13*8)) * -51)
print(72 % 8)  # this is the modulo operation
print(73 % 8)  # it returns the remainder of a division

# we can even "multiply" strings
print('The essence is ' + 'bla' * 3)

# now, comparing things is a very boolean thing to do
print('2 == 2? ', 2 == 2)
print('2 == 3? ', 2 == 3)
print('2 < 3? ', 2 < 3)
print('2 <= 2? ', 2 <= 2)
print('42 > 0? ', 42 > 0)
print('"lala" == "lala"', "lala" == "lala")

# comparing can be fun, IF you can do something with it
user_guess = input(f'Hey {user_name}, give me a number: ')
if user_guess == '42':
    print('Awesome, it seems there is nothing else left to lear for you')

user_guess = input(f'Gimme another one: ')
if user_guess == '42':
    print('Sneaky you, you are really intent on being stuck here, are you?')
else:
    print('Sorry, your number is just wrong!')

user_guess = input(f'Only one more: ')
if user_guess == '42':
    print('This is getting boring: ')
elif user_name == 'jackie':
    print(f'Well {user_name}, whatever you say seems to be the right thing.')
else:
    print('Not it! Well, maybe another time')

# so here we actually compared strings, not really numbers (as in int)
# but as we already have 100 lines of code here, let's try it out before we continue with basic2.py