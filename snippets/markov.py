import random

TEXT_FILE = 'frankenstein.txt'  # The name of the file with the training text
DEPTH = 6  # How many characters should be used for prediction of the next one
STARTER = 'The machine'  # Needs to be at least DEPTH characters long
LENGTH = 500  # Maximum length of characters for the resulting text.
DEBUG = False  # Print debug info (e.g. the transitions)

# read in the file line by line into a single string
text = ''
with open(TEXT_FILE, 'r') as file:
    for line in file:
        text += line
        # possible variation:
        # text += line.lower()

# now check for all DEPTH long transitions to a next character in the text
transitions = {}
for i in range(len(text) - DEPTH):
    precursor = text[i:i+DEPTH]
    next_char = text[i+DEPTH]
    # if we haven't found this recursor yet, add it to the dict
    if precursor not in transitions:
        transitions[precursor] = [next_char]  # here a set would be even more efficient
    # otherwise check if the next character was already parsed, and add a new one if not
    else:
        if next_char not in transitions[precursor]:
            transitions[precursor].append(next_char)

if DEBUG:
    for i in transitions:
        print(f'{repr(i)}', transitions[i])

# now generate a text from the collected transitions and the STARTER
generated_text = STARTER
while len(generated_text) < LENGTH:
    precursor = generated_text[-DEPTH:]
    if precursor not in transitions:
        break
    generated_text += random.choice(transitions[precursor])

# print our new machinic work of literary random art
print(generated_text)
