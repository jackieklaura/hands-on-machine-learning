# before we venture into machine land, we really should talk about
# lists, dicts, loops and functions

item = True
my_list = ['contains', 5, 'different', 'items like', item]
my_tuple = ('contains', 4, 'immutable', 'things')
my_list_from_tuple = list(my_tuple) + ['now mutable!']  # lists can be concatenated too
print(my_list_from_tuple, len(my_list_from_tuple))
# let's correct the number of items then
my_list_from_tuple[1] = len(my_list_from_tuple)
print(my_list_from_tuple)

# lists are indexed starting with 0. and they can be sliced
print(my_list[2:4])
print(my_list[2:])
print(my_list[:3])
print(my_list[1:2])  # isn't this a bit silly, as we could just use my_list[1] then?
print(my_list[1])    # hm ... see the difference?

# lists can be extended
print('extending my_list')
my_list.append('something')
my_list.extend([1, 2, 3])
my_list.insert(2, '(number is actually incorrect)')
print(my_list)

# and we can update single or multiple items
print('updating my_list')
my_list[1] = len(my_list)  # we've already seen this above
my_list[2:5] = ['(now correct)', 'very different', 'items']
my_list[7:] = [42, 10]  # this replaces part of the list with another (shorter) list
print(my_list)

# and we can also specifically delete items
print('deleting from my_list')
my_list.remove(10)
popped_item = my_list.pop(4)
print(f'popped_item: {popped_item} ; the list now looks like: {my_list}')
del my_list[1]
print(my_list)
my_list.clear()
print(my_list)
#del my_list
print(my_list)  # well, this should throw an error, becaus the list is gone now
# so remove the last line, before you move on to explore dictionaries
# and take a look at those many other aweseom list methods:
# https://www.w3schools.com/python/python_lists_methods.asp


my_dict = {'name': 'jackie', 'xp': 42, 'zombie': True}  # after such a speed run, what did you expect?
print(my_dict)
print(my_dict['name'])

# dicts contain key:value pairs. the values can also be dicts
my_dict = {
    'name': 'jackie',
    'data': {
        'xp': 42,
        'zombie': True,  # machine braaaaaains!
    },
}
print(my_dict)
print(my_dict['data'])
print(type(my_dict['data']))
print(my_dict['data']['zombie'])
print(my_dict.get('name'))  # a different way to access 
print(my_dict.get('something'))  # because my_dict['something'] would through an error

print('\nsome more ways to access dict keys and values:')
print(my_dict.values())
print(my_dict.keys())
print(my_dict.items())

print('\nadding and changing dict items')
my_dict['name'] = 'mafalda'
my_dict['data']['zombie'] = False
my_dict['a_new_key'] = 'a new value'  # adding something is easy as that
print(my_dict)
# but we can update (change/add) several key-value-pairs at once too
my_dict.update({'name': None, 'data': 'useless', 'hidden_gem': True})
print(my_dict)

print('\ndeleting works similar to lists')
popped_item = my_dict.pop('a_new_key')
print(f'popped_item: {popped_item} ; the dict now looks like: {my_dict}')
del my_dict['hidden_gem']
print(my_dict)
my_dict.clear()
print(my_dict)
#del my_dict
print(my_dict)
# well, this is same as with lists, same as with everything actually.
# if you delete it, it's gone. so fix this before you continue.
# also check out all of those dictionary methods:
# https://www.w3schools.com/python/python_dictionaries_methods.asp

print('\nOk, now, finally it is time to do some looping')
print("We'll loop through lists and dicts just for the fun of it")

my_list = ['contains', 5, 'different', 'items', True, 0.0, 1.234, False, None, {}, [], '']
for item in my_list:
    print('We now can do stuff with', item)
    if type(item) == str:
        print(f'This item is a string and of {len(item)} length.\n')
    elif type(item) == int:
        print('This item is an int:', item, '\n')
    else:
        output = 'This item appears to be something else.'
        if item:
            output += ' It looks like to be a truthy one.'
        else:
            output += ' Totally falsy of course.'
        print(output + '\n')
# here our loop ends

print('Looping through dicts works quite similarly')
my_dict = {'name': 'jackie', 'xp': 42, 'zombie': True, 'data': {'some': 'thing'}}
for key in my_dict:
    print('Currently at key', key, 'which has the value', my_dict[key])
# but sometimes it is more convenient to get the key and value as separate variables in the loop
for key, value in my_dict.items():
    print(f'In this loop we can access {key} and directly get {value}.')
    if type(value) == dict:
        print('A dict in a dict?!? Are you nuts? I wont look at this any further.')
    else:
        print('Boring')

# and because those loops actually work with all iterables we can also do this
for n in range(23):
    print(n)
else:
    print('imagine all the possibilities!')  # whait, what?!?
    # yep, that's a thing too, you can use an else after a for loop
    # look it up: https://www.w3schools.com/python/python_for_loops.asp


print('''

You've made it to the end!
I think this is enough for today.
Get some fresh air.
Take a deep breath.
Think about happy things.
Try not to become a robot until next Thursday.
Then we collect the missing pieces to become ... ah, create universal discrete state machines.
''')
from time import sleep
sleep(4)
print('\n\nOh...')
sleep(3)
print('by the way,')
sleep(3)
print('did you know this?\n')
sleep(2)
import this
