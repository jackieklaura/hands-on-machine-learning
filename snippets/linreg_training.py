import pandas as pd

VERBOSE = True

# our training data
file = 'ai_bs_trainingset.csv'

# read in the data frame from the CSV file and add a colum for ai_mentions / words
df = pd.read_csv(file, delimiter=';')
df['ai_per_words'] = df['ai_mentions'] / df['words']

# now randomly shuffle our data set (in case of doubt always a good practice before starting to train and validate)
df = df.sample(frac=1).reset_index(drop=True)

df = df.loc[df.type == 'journal'].reset_index(drop=True)

labels = df['bs_factor']
features = df['ai_per_words']

b = 0.0
w1 = 0.0
learning_rate = 0.1
epochs = 20


def calculate_mse(x, y, b, w1):
    """Calculate the L2 loss for a data set with given model parameters

    In this case we work with the following model: y = b + w1 * x1
    So we have just one feature (x1) which should be used to predict the label (y)
    As an error function the L2 loss is used and calculated as:
    loss = sum of all (ŷ - y)² for every x in the dataset

    :param x: the x values aka feature
    :param y: the y values aka labels
    :param b: the bias of our linear regression model
    :param w1: the weight of our linear regression model
    :return: the L2 loss (sum of all squared errors) for the provided data set
    """
    loss = 0
    for i in range(len(y)):
        y_hat = b + w1 * x[i]
        loss += (y_hat - y[i]) ** 2
    return loss


for e in range(epochs):
    # calculate the error (loss) for the current model
    error = calculate_mse(features, labels, b, w1)
    # calculate the error (loss) for adapted weights
    error_w1_plus = calculate_mse(features, labels, b, w1 + learning_rate)
    error_w1_minus = calculate_mse(features, labels, b, w1 - learning_rate)
    # calculate the error (loss) for adapted bias
    error_b_plus = calculate_mse(features, labels, b + learning_rate, w1)
    error_b_minus = calculate_mse(features, labels, b - learning_rate, w1)

    # in case we want to see what is going on, provide some output
    if VERBOSE:
        print(f'-------------------------\nEpoch {e}:')
        print(f'current b/w1: {b:{2}.{3}} / {w1:{2}.{3}}   error: {error}')

    # now adjust the values accordingly
    if error_w1_plus < error_w1_minus:
        w1 += learning_rate
    else:
        w1 -= learning_rate
    if error_b_plus < error_b_minus:
        b += learning_rate
    else:
        b -= learning_rate

    # and also output the adapted values in verbose mode
    if VERBOSE:
        print(f'adapted b/w1: {b:{2}.{3}} / {w1:{2}.{3}}')

# print the results
print(f'\n\n-------------------------\nCalculation completed after {e+1} epochs ')
print(f'The final model: y_hat = {b} + {w1} * x1')
print(f'Have fun predicting some bs!')
