from knn_data import dataset
from math import sqrt


def get_distance(item1, item2):
    """Returns the distance between to data points.

    The items have to be dictionaries containing values for cuteness and fluffiness.
    """
    dx = item1['cuteness'] - item2['cuteness']
    dy = item1['fluffiness'] - item2['fluffiness']
    return sqrt(dx**2 + dy**2)


def get_neighbours(item, k, dataset):
    """Returns the k nearest neighbours of item in dataset.

    :param item: The item to find the nearest neighbours for
    :param k: The number of nearest neighbours to find
    :param dataset: The dataset to find neighbours in
    :return: a list with the k nearest neighbours
    """
    all_diffs = [(get_distance(item, d), d) for d in dataset]
    sorted_diffs = sorted(all_diffs, key=lambda x: x[0])
    sorted_dataset = [x[1] for x in sorted_diffs]
    return sorted_dataset[0:k]


def classify(item, k, dataset):
    """Returns the classification of an unknown item.

    :param item: The item to classify
    :param k: The number of neighbours used to classify the item
    :param dataset: The dataset used to classify the item
    :return: Either the classified type of the item or "inconclusive"
    """
    neighbours = get_neighbours(item, k, dataset)
    rank = {}
    for neighbour in neighbours:
        if neighbour['type'] not in rank:
            rank[neighbour['type']] = 1
        else:
            rank[neighbour['type']] += 1
    ranked = sorted(rank.items(), key=lambda x: x[1], reverse=True)
    if len(ranked) == 1 or ranked[0][1] > ranked[1][1]:
        return ranked[0][0]
    else:
        return 'inconclusive'


# now let's define some new items, which we don't know the type of yet
new_items = [
    {'cuteness': 0, 'fluffiness': 0},
    {'cuteness': 1, 'fluffiness': 0},
    {'cuteness': 0, 'fluffiness': 1},
    {'cuteness': 1, 'fluffiness': 1},
    {'cuteness': 0.5, 'fluffiness': 0.5},
    {'cuteness': 0.5, 'fluffiness': 0.46},
    {'cuteness': 0.61, 'fluffiness': 0.5},
]

# and for each item print the prediction based on the dataset
for item in new_items:
    print(classify(item, 5, dataset))
