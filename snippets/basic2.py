# as we are getting into error handling now, let's define a DEMO var
DEMO = True

try:
    print(23 + '23')
except Exception as err:
    if DEMO:
        print("Ugh, there was an error. But never mind, we'll just continue")
    else:
        raise err

print(str(23) + '23')
print(23 + int(23))

# Ok, so type conversion is an important thing. Let's try that with our
# number guesser
guess = input('Hey user, gimme a number: ')
# You might get an error here, depending on your input
try:
    if int(guess) == 42:
        print('Correct!')
    else:
        print('Not it!')
except:
    print("Ugh, there was an error. But never mind, we'll just continue")

# Errors sometimes seem ugly and annoying,
# but really, don't be a snob, they can be your friends
# Turn off the DEMO mode and see what you get.
# Try to fix the code one way or another.

# Now which of the following produce an error?
try:
    print(12 + 12)
    print('12' + '12')
    print('12' + 12)
    print('12 + 12')
    print(2 * 5)
    print('2' * '5')
    print('2' * 5)
    print('2 * 5')
except:
    if DEMO:
        print("Ugh, there was an error. But never mind, we'll just continue")
    else:
        raise err

# we've already seen type conversion, here some more examples:
variable_1 = 12
variable_2 = '12'
str(variable_1)
int(variable_2)
float(variable_2)

# now we can actually check the user input more properly
guess = input('Hey user, gimme a number again: ')
try:
    guess = int(guess)
except:
    print("Come on, why can't you give me a number?")
else:
    if guess > 10000:
        print("That's quite a large number")
    else:
        print('Nothing special about this number')
# This does not handle ALL numbers. Can you fix it?

print('\nAnd now for something completely different!\n')
print('A side note on string formatting:')

name = 'jackie'
day = 'Thursday'

# simple string concatenation, as we already know it
formatted = 'Hello ' + name + ', what a ' + day + '!'
print(formatted)

# the old style - good to know but please don't use it
formatted = 'Hello %s, what a %s!' % (name, day)
print(formatted)

# the new style
formatted = 'Hello {}, what a {}!'.format(name, day)
print(formatted)

# f-strings : the best, since Python 3.6
formatted = f'Hello {name}, what a {day}!'
print(formatted)

# Still, everything is so boringly determined here. Let's add some randomness
# and use some standard library features
from random import randint
from datetime import datetime

number = randint(0, 5)
now = datetime.now()
weekday = now.weekday()

print('Today, right now, this moment is', now.strftime('%A, %Y-%m-%d %H:%M')) 
if abs(weekday - number) == 0:
    print('Wow! This is a perfect day!')
elif abs(weekday - number) <= 2:
    print('A day as every other.')
else:
    print('This day really sucks!')
  
# check out https://www.w3schools.com/python/python_datetime.asp
# for more things you can do with dates

# enough for now. try to get this script running. play around with it.
# in DEMO and outside DEMO mode. fix the errors. and then ready yourself
# for part 3