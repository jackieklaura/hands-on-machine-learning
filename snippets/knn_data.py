# adopted from dataset of example in chapter 8 of "Künstliche Intelligenz verstehen"
# https://www.maschinennah.de/ki-buch/
dataset = [
  {'cuteness': 0.21, 'fluffiness': 0.91, 'type': 'tarantula'},
  {'cuteness': 0.27, 'fluffiness': 0.80, 'type': 'tarantula'},
  {'cuteness': 0.15, 'fluffiness': 0.75, 'type': 'tarantula'},
  {'cuteness': 0.37, 'fluffiness': 0.87, 'type': 'tarantula'},
  {'cuteness': 0.29, 'fluffiness': 0.70, 'type': 'tarantula'},

  {'cuteness': 0.90, 'fluffiness': 0.90, 'type': 'bunny'},
  {'cuteness': 0.86, 'fluffiness': 0.80, 'type': 'bunny'},
  {'cuteness': 0.75, 'fluffiness': 0.84, 'type': 'bunny'},
  {'cuteness': 0.95, 'fluffiness': 0.75, 'type': 'bunny'},
  {'cuteness': 0.70, 'fluffiness': 0.65, 'type': 'bunny'},

  {'cuteness': 0.31, 'fluffiness': 0.22, 'type': 'shark'},
  {'cuteness': 0.14, 'fluffiness': 0.13, 'type': 'shark'},
  {'cuteness': 0.21, 'fluffiness': 0.06, 'type': 'shark'},
  {'cuteness': 0.11, 'fluffiness': 0.25, 'type': 'shark'},
  {'cuteness': 0.33, 'fluffiness': 0.11, 'type': 'shark'},

  {'cuteness': 0.90, 'fluffiness': 0.10, 'type': 'hedgehog'},
  {'cuteness': 0.80, 'fluffiness': 0.17, 'type': 'hedgehog'},
  {'cuteness': 0.70, 'fluffiness': 0.11, 'type': 'hedgehog'},
  {'cuteness': 0.75, 'fluffiness': 0.26, 'type': 'hedgehog'},
  {'cuteness': 0.92, 'fluffiness': 0.22, 'type': 'hedgehog'}
]
