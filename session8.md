# Session 8: Linear regression continued

In this session we will continue with linear regression. While last time we did it
with an algorithm crafted from scratch, this time we will look into how the Google
Machine Learning Crash Course does that by using TensorFlow. Go to the chapter
[First Steps with TF](https://developers.google.com/machine-learning/crash-course/first-steps-with-tensorflow)
and work through the programming exercises.

In order to work on the interactive exercises on Google Colab, you need to have an
account with Google. If you do not want to do that, you can alternatively set up
[Jupyter Notbooks](http://jupyter.org/install) and use the .ipynb files in our
base cloud folder for this course.

After some experimentation we will discuss the different approaches to learning
machine learning, that we have encountered by now.

## Exercise 3 - Go with the tensor flow

In this final exercise you will again find your own data set. You can also take
the one you have chosen for exercise 2, but you will have to decide whether this
fits your machine learning approach. The goal of this exercise is to use the
Tensorflow library to do some linear regression learning on your data set or to
experiment with other learning approaches (if you want to work through the rest
of the Google Crash Course).

The final product of this exercise should be a single Python script (not a
Jupyter notebook), that reads in a data set and uses TensorFlow to learn something
about the data set. You can decide yourself what you and the machine want to learn
and how you want to do it. As usual, you can upload your script to the
`Exercise 3` folder in our base cloud course folder.

**Exercise submission & deadline:**
 Hand in this assignment until Wed, 7th June 2023 23:42 to get the full points - or ask for an
 extension beforehand. Late submission (if not asked for an extension beforehand) will only get
 you half the points.

 To hand in the exercise name your script in the following format: `f'{student_id}_{firstname}_{lastname}.py'`
 and upload it to the `Exercise 3` folder of our base cloud course folder.
