# Session 9: Excursion

This session will be used to do an excursion to the [E-Day 23 on "AI"](https://www.wko.at/Content.Node/kampagnen/E-Day/programm.html).
As the talks are in German and our course time only covers one talk, participation in this "excursion" is not
mandatory. Details will be posted in our baseChat channel, and there will be an alternative way to participate in
our discourse.
