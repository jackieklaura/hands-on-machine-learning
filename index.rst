Documentation of the VU Hands on Machine Learning
=================================================

This is the documentation of the
`VU 2.0 ECTS Hands on Machine Learning <https://base.uni-ak.ac.at/courses/2024S/S04593/>`_
course at the University of Applied Arts Vienna (short: dieAngewandte). This is the
second (and still experimental) iteration of this course in summer term 2024. For
the archived documentation of the first instance in 2023 go to `../2023 <../2023>`_.

The general schedule for this course (all sessions from 14:00 to 18:00):

* 11 March 2024, SR 35 : Common kick-off session with SE Machine Learning
* 25 March 2024, SR 35 : optional session for coding newcomers
* 22 April 2024, SR 35 : data reading and analysis with pandas
* 06 May 2024, SR 35 : simple machine learning approaches from scratch: markov chains, linear regression
* 03 June 2024, SR 35 : more machine learning from scratch - KNN & k-means clustering, q-learner
* 10 June 2024, SR 35 : neural networks from scratch & common machine learning libraries
* 17 June 2024, SR 33 : final project planning, recap & course reflection (Attention: different seminar room)

Refer to the session chapters to follow up on what we did in the specific
sessions, and check out the general references section for further material.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   session1
   self-learning
   pandas
   markov_and_linreg
   knn_and_k-means
   neural_networks
   exercises
   references
   grading


This documentation was produced with `Sphinx <https://www.sphinx-doc.org>`_,
the *Python Documentation Generator*. Sphinx itself is written in Python and can
be installed with ``pip install sphinx``. Additionally, for this specific docs,
also the `MyST Parser <https://myst-parser.readthedocs.io>`_ is used in order
to write the section files in `Markdown <https://commonmark.org/>`_.
`linkify-it-py <https://myst-parser.readthedocs.io/en/latest/syntax/optional.html#linkify>`_
is used so that URLs are automagically converted to links. For the theme I am
relying on the good old ``sphinx_rtd_theme``, which is used by many open source
projects for their documentation on `readthedocs.io <https://readthedocs.io>`_.
This is extended by ``sphinx_rtd_dark_mode``, so that readers get the option
to switch between a light and dark mode (see the round button with the moon/sun
symbole in the lower right corner of the browser tab). Theme related links can
be found in the footer..

The source of this documentation, including the sphinx config used to generate
this output, is available under a CC-BY-SA 4.0 license on
https://gitlab.com/jackieklaura/hands-on-machine-learning
